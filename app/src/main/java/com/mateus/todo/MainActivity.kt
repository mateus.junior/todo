package com.mateus.todo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.textfield.TextInputLayout
import com.mateus.todo.model.Task
import com.mateus.todo.ui.list.ToDoAdapter

class MainActivity : AppCompatActivity() {
    private lateinit var tasks: ArrayList<Task>
    private lateinit var txt_InputTask: TextView
    private lateinit var isUrgent: SwitchCompat
    private lateinit var toDoAdapter:ToDoAdapter
    private lateinit var rvTaskList: RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.isUrgent=findViewById(R.id.switchUrgent)
        this.txt_InputTask = findViewById(R.id.txtInputEditText)
        this.tasks = ArrayList()
        this.rvTaskList = findViewById(R.id.recyclerView)
        this.rvTaskList.layoutManager = LinearLayoutManager(this)
        this.toDoAdapter = ToDoAdapter(this.tasks)
        this.rvTaskList.adapter = this.toDoAdapter

    }
    fun onCLickAddTask(v:View){
        val str_task: String = this.txt_InputTask.text.toString()
        if(isUrgent.isChecked){
            if(str_task.isNotEmpty())
            {
                val todo = Task( str_task, isDone = false, isUrgent = true)
                this.toDoAdapter.addTask(todo)
                this.txt_InputTask.setText("")
            }
        }else{
            if(str_task.isNotEmpty())
            {
                val todo = Task( str_task, isDone = false, isUrgent = false)
                this.toDoAdapter.addTask(todo)
                this.txt_InputTask.setText("")
            }
        }
    }
}