package com.mateus.todo.ui.list

import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mateus.todo.R
import com.mateus.todo.model.Task
import java.security.KeyStore

class ToDoViewHolder (
    itemView: View,
    private val adapter: ToDoAdapter
):RecyclerView.ViewHolder(itemView){
    private val SwitchDone: SwitchCompat
    private val TextTask: TextView
    private val flUrgente: FrameLayout
    private lateinit var currentTask: Task
    init{
        this.SwitchDone=itemView.findViewById((R.id.switchItemview))
        this.TextTask=itemView.findViewById(R.id.txtItemTask)
        this.flUrgente = itemView.findViewById(R.id.fl_Itemview)
        SwitchDone.setOnCheckedChangeListener{_, done ->
            currentTask.isDone = done

        }
    }

    fun bind(task: Task){
        this.currentTask = task
        this.TextTask.text = this.currentTask.txt_task
        this.SwitchDone.isChecked = this.currentTask.isDone
        val color = if (this.currentTask.isUrgent){
            ContextCompat.getColor(itemView.context,R.color.red)
        }else{
            ContextCompat.getColor(itemView.context,R.color.green)
        }
        flUrgente.background.setTint(color)

    }


}