package com.mateus.todo.ui.list

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Adapter
import androidx.recyclerview.widget.RecyclerView
import com.mateus.todo.R
import com.mateus.todo.model.Task

class ToDoAdapter(
    private val tasks: ArrayList<Task>
): RecyclerView.Adapter<ToDoViewHolder>()  {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ToDoViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemView = layoutInflater.inflate(
            R.layout.item_view,
            parent,
            false
        )
        return ToDoViewHolder(itemView,this)
    }

    override fun onBindViewHolder(holder: ToDoViewHolder, position: Int) {
        holder.bind(this.tasks[position])
    }

    override fun getItemCount(): Int {
        return  this.tasks.size
    }
    fun addTask(listTask:Task)
    {
        tasks.add(listTask)
        notifyItemInserted(tasks.size-1)
    }

}
